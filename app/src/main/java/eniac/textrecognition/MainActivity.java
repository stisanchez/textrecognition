package eniac.textrecognition;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.Text;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

public class MainActivity extends AppCompatActivity {

     Button button;
     TextView txtView;
     ImageView imageView;
     Bitmap bitmap;
    Spinner spinner;


    private int getIdImage(){
        int result = 0;
        switch (spinner.getSelectedItemPosition()){
            case 0:
                result = R.drawable.segundo;
                break;
            case 1:
                result = R.drawable.imagen;
                break;
            case 2:
                result = R.drawable.tercera;
                break;
        }
        return result;
    }


    private void changeImage(){
        // To get bitmap from resource folder of the application.
        bitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(), getIdImage());
        imageView.setImageResource(getIdImage());
        // Starting Text Recognizer
        TextRecognizer txtRecognizer = new TextRecognizer.Builder(getApplicationContext()).build();
        if (!txtRecognizer.isOperational())
        {
            // Shows if your Google Play services is not up to date or OCR is not supported for the device
            txtView.setText("Detector dependencies are not yet available");
        }
        else
        {
            // Set the bitmap taken to the frame to perform OCR Operations.
            Frame frame = new Frame.Builder().setBitmap(bitmap).build();
            SparseArray items = txtRecognizer.detect(frame);
            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < items.size(); i++)
            {
                TextBlock item = (TextBlock)items.valueAt(i);
                strBuilder.append(item.getValue());
                strBuilder.append("/");
                // The following Process is used to show how to use lines & elements as well
                for (int j = 0; j < items.size(); j++) {
                    TextBlock item2 = (TextBlock) items.valueAt(j);
                    strBuilder.append(item2.getValue());
                    strBuilder.append("/");
                    for (Text line : item2.getComponents()) {
                        //extract scanned text lines here
                        Log.v("lines", line.getValue());
                        for (Text element : line.getComponents()) {
                            //extract scanned text words here
                            Log.v("element", element.getValue());
                        }
                    }
                }
            }
            txtView.setText(strBuilder.toString());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtView = findViewById(R.id.txtView);
        imageView = findViewById(R.id.image_view);
        spinner = (Spinner) findViewById(R.id.spinner);
        String[] letra = {"Imagen 1","Imagen 2","Imagen 3"};
        spinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, letra));


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id)
            {
                changeImage();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {  }
        });

        changeImage();

        /*button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/
    }
}
